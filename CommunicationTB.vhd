LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
 
ENTITY CommunicationTB IS
END CommunicationTB;
 
ARCHITECTURE test OF CommunicationTB IS  

	COMPONENT controller
	PORT(rst, clk, start, data_in : IN  std_logic;
		  data_out : OUT  std_logic_vector(7 downto 0);
		  done : OUT  std_logic);
	END COMPONENT;
    
	SIGNAL rst : std_logic := '0';   
	SIGNAL clk : std_logic := '0';
   SIGNAL start : std_logic := '0';
   SIGNAL data_in : std_logic := '0';

   SIGNAL data_out : std_logic_vector(7 DOWNTO 0);
   SIGNAL done : std_logic;

   CONSTANT clk_period : TIME := 10 NS;
	FOR ALL: controller USE ENTITY WORK.Communication(Behavioral);

BEGIN
	uut: controller PORT MAP(rst, clk, start, data_in, data_out, done);
		
   clk_process: PROCESS
   BEGIN
		clk <= '0';
		WAIT FOR clk_period/2;
		clk <= '1';
		WAIT FOR clk_period/2;
   END PROCESS clk_process;
 

	proc: PROCESS
	BEGIN 
		rst <= '1';
		WAIT FOR clk_period;
		rst <= '0';
		start <= '1';
		WAIT FOR clk_period;
		data_in <= '1';
		start <= '0';
		WAIT FOR clk_period;
		data_in <= '0';
		WAIT FOR clk_period;
		data_in <= '1';
		WAIT FOR clk_period*10;
		
		start <= '1';
		WAIT FOR clk_period;
		data_in <= '0';
		start <= '0';
		WAIT FOR clk_period;
		data_in <= '1';
		WAIT FOR clk_period;
		data_in <= '0';
		WAIT FOR clk_period;
		data_in <= '1';
		WAIT FOR clk_period*20;
		WAIT;
	END PROCESS proc;
END test;
