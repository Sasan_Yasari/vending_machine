LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY main IS
	PORT(rst, clk, money, type1, type2, type3, type4, start, data_in: IN STD_LOGIC;
		  done, outOfService, busy, p1, p2, p3, p4: OUT STD_LOGIC;
		  empty: OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END main;

ARCHITECTURE Behavioral OF main IS
	COMPONENT com PORT(rst, clk, start, data_in:IN STD_LOGIC;
						    data_out: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
							 done: OUT STD_LOGIC);
	END COMPONENT;
	TYPE states IS (state1, state2, state3, state4, state5, state6); 
	SIGNAL cur_state, next_state : states := state1;
	SIGNAL en, comDone: STD_LOGIC;
	SIGNAL types: STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL countP1, countP2, countP3, countP4 : INTEGER RANGE 0 TO 7;
	SIGNAL comOut: STD_LOGIC_VECTOR(7 DOWNTO 0);
	FOR ALL: com USE ENTITY WORK.Communication(Behavioral);
BEGIN
	G0: com PORT MAP(rst, clk, en, data_in, comOut, comDone);
	
	SYNC: PROCESS(clk)
	BEGIN
		IF(clk'event AND clk = '1') THEN
			IF(rst = '1') THEN
				cur_state <= state1;
			ELSE
				cur_state <= next_state;
			END IF;
		END IF;
	END PROCESS SYNC;
	
	OUTPUT: PROCESS(cur_state, clk)
	BEGIN 
		busy <= '1';
		IF(cur_state = state1) THEN
			outOfService <= '1';
			empty <= "1111";
			p1 <= '0';
			p2 <= '0';
			p3 <= '0';
			p4 <= '0';
		ELSIF(cur_state = state4) THEN
			CASE(comOut(1 DOWNTO 0)) IS
				WHEN "00" =>
					countP1 <= TO_INTEGER(UNSIGNED(comOut(4 DOWNTO 2)));
					IF(countP1 > 0) THEN
						empty(0) <= '0';
					END IF;
				WHEN "01" =>
					countP2 <= TO_INTEGER(UNSIGNED(comOut(4 DOWNTO 2)));
					IF(countP2 > 0) THEN
						empty(1) <= '0';
					END IF;
				WHEN "10" =>
					countP3 <= TO_INTEGER(UNSIGNED(comOut(4 DOWNTO 2)));
					IF(countP3 > 0) THEN
						empty(2) <= '0';
					END IF;
				WHEN "11" =>
					countP4 <= TO_INTEGER(UNSIGNED(comOut(4 DOWNTO 2)));
					IF(countP4 > 0) THEN
						empty(3) <= '0';
					END IF;
				WHEN OTHERS =>
					countP1 <= TO_INTEGER(UNSIGNED(comOut(4 DOWNTO 2)));
			END CASE;
		ELSIF(cur_state = state5) THEN
			p1 <= '0' AFTER 10 NS;
			p2 <= '0' AFTER 10 NS;
			p3 <= '0' AFTER 10 NS;
			p4 <= '0' AFTER 10 NS;
			outOfService <= '0';
			busy <= '0';
		ELSIF(cur_state = state6 and (clk'event and clk = '1')) THEN
			IF(types(0) = '1') THEN
				IF(countP1 = 0) THEN
					empty(0) <= '1';
					p1 <= '0';
				ELSE
					countP1 <= countP1 - 1;
					p1 <= '1';
				END IF;
			ELSIF(types(1) = '1') THEN
				IF(countP2 = 0) THEN
					empty(1) <= '1';
					p2 <= '0';
				ELSE
					countP2 <= countP2 - 1;
					p2 <= '1';
				END IF;
			ELSIF(types(2) = '1') THEN
				IF(countP3 = 0) THEN
					empty(2) <= '1';
					p3 <= '0';
				ELSE
					countP3 <= countP3 - 1;
					p3 <= '1';
				END IF;
			ELSIF(types(3) = '1') THEN
				IF(countP4 = 0) THEN
					empty(3) <= '1';
					p4 <= '0';
				ELSE
					countP4 <= countP4 - 1;
					p4 <= '1';
				END IF;
			END IF;
		END IF;	
	END PROCESS OUTPUT;
	
	NEXT_ST: PROCESS(cur_state, en, start, money, comDone)
	BEGIN
		done <= '0';
		next_state <= cur_state;
		CASE(cur_state) IS
			WHEN state1 =>
				IF(start = '1') THEN
					next_state <= state2;
				END IF;
			WHEN state2 =>
				IF(start = '0') THEN
					en <= '1';
					next_state <= state3;
				END IF;
			WHEN state3 =>
				en <= '0' AFTER 10 NS;
				IF(comDone = '1') THEN
					next_state <= state4;
				END IF;
			WHEN state4 =>
				next_state <= state5;
				done <= '1';
			WHEN state5 =>
				IF(start = '1') THEN
					next_state <= state2;
				ELSIF(money = '1') THEN 
					types(0) <= type1;
					types(1) <= type2;
					types(2) <= type3;
					types(3) <= type4;
					next_state <= state6;
				END IF;
			WHEN state6 =>
				next_state <= state5;
		END CASE;
	END PROCESS NEXT_ST;
	
END Behavioral;

