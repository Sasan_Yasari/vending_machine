LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE IEEE.NUMERIC_STD.ALL;

ENTITY Communication IS
	PORT(rst, clk, start, data_in: IN STD_LOGIC;
		  data_out: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		  done    : OUT STD_LOGIC);
END Communication;

ARCHITECTURE Behavioral OF Communication IS
	TYPE states IS (state0, state1, state2, state3);
	SIGNAL cur_state, next_state: states := state0;
	SIGNAL counter: STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL output:  STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
	SYNC: PROCESS(clk)
	BEGIN
		IF(clk'event AND clk = '0') THEN
			IF(rst = '1') THEN
				cur_state <= state1;
			ELSE
				cur_state <= next_state;
			END IF;
		END IF;
	END PROCESS SYNC; 
	
	OUTPUT_PROCESS: PROCESS(clk)
	BEGIN
		IF(clk'event and clk = '1') THEN
			IF(next_state = state2) THEN 
				counter <= counter + '1';
				output(TO_INTEGER(UNSIGNED(counter))) <= data_in;
			ELSIF(next_state = state3) THEN 
				data_out <=output;
				done <= '1';
			ELSIF(next_state = state0) THEN
				done <= '0';
				counter <= "0000";
			END IF;
		END IF;
	END PROCESS OUTPUT_PROCESS;
	
	NEXT_ST: PROCESS(cur_state, start, clk)
	BEGIN
		next_state <= cur_state;
		CASE(cur_state) IS
			WHEN state0 => 
				IF start = '1' THEN
					next_state <= state1;
				END IF;
			WHEN state1 => 
				IF start = '0' THEN
					next_state <= state2;
				END IF;
			WHEN state2 =>
					IF(counter > "0111") THEN
						next_state <= state3;
					END IF;
			WHEN state3 =>
				next_state <= state0;
		END CASE;
	END PROCESS NEXT_ST;
	
END Behavioral;

