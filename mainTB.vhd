LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
 
ENTITY mainTB IS
END mainTB;
 
ARCHITECTURE test OF mainTB IS 

    COMPONENT main
    PORT(rst, clk, money, type1, type2, type3, type4, start, data_in: IN  STD_LOGIC;
         done, outOfService, busy, P1, P2, P3, P4 :OUT STD_LOGIC;
         empty : OUT  STD_LOGIC_VECTOR(3 DOWNTO 0));
    END COMPONENT;

   SIGNAL rst, clk, money, type1, type2, type3, type4, start, data_in : STD_LOGIC := '0';
   SIGNAL done,outOfService, busy, P1, P2, P3, P4 : STD_LOGIC;
   SIGNAL empty : STD_LOGIC_VECTOR(3 DOWNTO 0);

   CONSTANT clk_period : TIME := 10 NS;
	FOR ALL: main USE ENTITY WORK.main(Behavioral);
BEGIN
 
   uut: main PORT MAP(rst, clk, money, type1, type2, type3, type4, start,data_in,
							 done, OutOfService, busy, p1, p2, p3, p4, empty);

   clk_process :PROCESS
   BEGIN
		clk <= '0';
		WAIT FOR clk_period/2;
		clk <= '1';
		WAIT FOR clk_period/2;
   END PROCESS;
 

   proc: PROCESS
	BEGIN 
		--t1
		rst <='1';
		WAIT FOR clk_period;
		rst <='0';
		WAIT FOR clk_period;
		start <= '1';
		WAIT FOR clk_period;
		start <= '0';
		data_in <= '0';
		WAIT FOR clk_period*4;
		data_in <= '1';
		WAIT FOR clk_period;
		data_in <= '0';
		WAIT FOR clk_period*4;
		--t2
		start <= '1';
		WAIT FOR clk_period*5;
		start <= '0';
		WAIT FOR clk_period;
		data_in <= '1';
		WAIT FOR clk_period;
		data_in <= '0';
		WAIT FOR clk_period*2;
		data_in <= '1';
		WAIT FOR clk_period;
		data_in <= '0';
		--order t1
		WAIT FOR clk_period*10;
		type1 <= '1';
		money <= '1';
		WAIT FOR clk_period;
		money <= '0';
		type1 <= '0';
		WAIT FOR clk_period*10;
		--order t3
		type3 <= '1';
		money <= '1';
		WAIT FOR clk_period;
		money <= '0';
		type3 <= '0';
		WAIT FOR clk_period*10;
		WAIT;
	END PROCESS proc;

END test;
